function problem1(inventory ,id ){
    let car_matched = [];
    if (inventory == undefined || id == undefined){
       return [] ;
    }
    else if (id <1 || id >=51 || inventory.length==0){
        return [] ;
    }
    for(let i = 0; i<inventory.length; i++){ 
        if(inventory[i].id == id){
            let match_str = "Car "+id+" is " +inventory[i].car_year+" "+ inventory[i].car_make + " " + inventory[i].car_model
             car_matched = match_str.split(" ");
             return car_matched ;
        }
    }
}

module.exports = problem1;