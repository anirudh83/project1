function problem6(inventory = [], car1="", car2=""){
    let BMWAndAudi = [];
    if (inventory.length == 0 ||car1 == "" || car2 == ""){
        return [] ;
    }
    for(let i = 0; i<inventory.length; i++){
        if (inventory[i].car_make == car1 || inventory[i].car_make == car2){
            let car_object={
                'car_makes' : inventory[i].car_make,
                'car_model' : inventory[i].car_model
            }
            BMWAndAudi.push(car_object);    
        }
        
    }
    return  JSON.stringify(BMWAndAudi);
}
    

module.exports = problem6;