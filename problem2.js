function problem2(inventory){
    if ( inventory == undefined){
        return [];
    }
    else if(inventory.length==0){
        return [];
    }
    else{
        length = inventory.length;
        let last_car_details = "Last car is a "+inventory[length-1].car_make + " "+ inventory[length-1].car_model 
        let last_car=last_car_details.split(" ");
        return last_car ;
    }
    
}

module.exports = problem2;