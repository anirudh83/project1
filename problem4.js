let car_years = [];
function problem4(inventory = []){ 
    if (inventory.length == 0){
        return [] ;
    }
    for(let i = 0; i < inventory.length; i++){
        car_years.push(inventory[i].car_year); 
    }
   return car_years;
}

exports.car_years = car_years;
exports.problem4 = problem4;